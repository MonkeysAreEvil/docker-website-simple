FROM monkeysareevil/stretch
MAINTAINER MonkeysAreEvil

# Update this to reflect your actual domain
# Alternatively, define this in your docker-compose.yml
ENV SERVER_NAME=example.com
ENV SERVER_EMAIL=admin@example.com

# Packages
RUN apt-get update -q --fix-missing
RUN apt-get -y upgrade
RUN apt-get -y install apache2

# Setup apache
ADD website.tmpl /etc/apache2/sites-available
CMD /bin/sh -c "envsubst < /etc/apache2/sites-available/website.tmpl > /etc/apache2/sites-available/website.conf"
RUN a2ensite website
RUN a2dissite 000-default
RUN a2enmod ssl

# Web port
EXPOSE 443

# Start-mailserver script
ADD start-website.sh /usr/local/bin/start-website.sh
RUN chmod +x /usr/local/bin/start-website.sh
CMD /usr/local/bin/start-website.sh
