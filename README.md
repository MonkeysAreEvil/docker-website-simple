# Description

A Docker container for running a simple website (e.g. my blog). It is TLS enabled.

# Install

1| Build a stretch base image.

```
emerge debootstrap
debootstrap stretch stretch > /dev/null
tar -C stretch -c . | docker import - monkeysareevil/stretch
```

2| Grab some Let's Encrypt certificates, and a domain.

3| Build the container. Be sure to inspect the Dockerfile and change the `FROM` and `ENV` directives as required.

```
docker build -t website.
```

4| Set up any proxy, firewall, or router settings as required. For example, I use an [nginx proxy](https://MonkeysAreEvil.bitbucket.com/docker-nginx-proxy) for all my web containers. This is also Dockerised.

# Licence

GPL v3+
